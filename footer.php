<footer>
    <div class="container">
        <p>
           Yiğidin Malı Meydandadır <a href="https://www.gitlab.com/efezahid/minimal-wp-theme">Temayı İndirebilirsiniz</a>
            - İletişimde Kalın -
            <?php echo get_option('facebook_handler') ? '<a href="'.get_option('facebook_handler').'"><i class="fa fa-facebook"></i></a>':null; ?>
            <?php echo get_option('twitter_handler') ? '<a href="'.get_option('twitter_handler').'"><i class="fa fa-twitter"></i></a>':null; ?>
            <?php echo get_option('youtube_handler') ? '<a href="'.get_option('youtube_handler').'"><i class="fa fa-youtube"></i></a>':null; ?>
            <?php echo get_option('instagram_handler') ? '<a href="'.get_option('instagram_handler').'"><i class="fa fa-instagram"></i></a>':null; ?>
            <?php echo get_option('codepen_handler') ? '<a href="'.get_option('codepen_handler').'"><i class="fa fa-codepen"></i></a>':null; ?>
        </p>
    </div>
</footer>
<link rel="stylesheet" href="<?php bloginfo("template_url")?>/css/style_2.css">
<link rel="stylesheet" href="<?php bloginfo("template_url") ?>/css/font-awesome.min.css">
<script src="<?php bloginfo("template_url") ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo("template_url") ?>/js/custom.js"></script>
<?php wp_footer(); ?>
</body>
</html>