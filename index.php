<?php get_header(); ?>



	<div class="header-text">
		<h1><?php bloginfo("name"); ?></h1>
		<h4><?php bloginfo("description"); ?></h4>
	</div>

	<div class="header-image">
		<img src="<?php bloginfo("template_url") ?>/images/header.jpg" alt="Mustafa Zahid Efe">
	</div>
</header>

<section id="blogs">
	<div class="container">
		<?php isset($_GET["s"]) ? print('<h1 style="text-align:center;"><span style="color:#c0392b">'.htmlentities($_GET["s"]).'</span> İçin Arama Sonuçları</h1>') : null; ?>
		<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<div class="blog">
			<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
			<small>
				<span><i class="fa fa-list-ul"></i> <?php the_terms(get_the_ID(), "category"); ?></span>
				<span><i class="fa fa-star-o"></i> 15</span>
				<span><i class="fa fa-comment-o"></i> <?php comments_number("Yorum Yok", "1 Yorum", "% Yorum"); ?></span>
			</small>
			<p><?php the_excerpt(); ?></p>
			<a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
		</div>
		<?php endwhile; else: ?>
		<h1 style="text-align:center;">Herhangi Bir İçerik Bulunamadı!</h1>
		<?php endif; ?>
		<?php pagination(); ?>

		<!--<ul class="pagination">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
		</ul>-->
	</div>
</section>

<?php get_footer(); ?>