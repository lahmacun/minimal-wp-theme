<div class="comments">
    <?php comment_form([
        "fields" => apply_filters("comment_form_default_fields", array(
            'author' => "<label>Adınız <input type='text' name='author'></label>",
            'email' => "<label>Eposta Adresiniz <input type='email' name='email'></label>"
        )),
        "id_form" => "",
        "id_submit" => "",
        "title_reply" => "Cevapla",
        "title_reply_to" => "Cevapla %s",
        "label_submit" => "Yorumu Gönder",
        "comment_field" => "<label>Yorumunuz <textarea name='comment'></textarea></label>",
        "class_submit" => "btn btn-primary",
        "comment_notes_before" => ""
    ]); ?>
</div>