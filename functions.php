<?php

function pagination($pages = '', $range = 3)
{
    $showitems = ($range * 2)+1;
    global $paged;
    if(empty($paged)) $paged = 1;
    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }
    if(1 != $pages)
    {
        echo "<ul class='pagination'>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&laquo;</a>";
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<li class='current'>".$i."</li>":"<li><a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a></li>";
            }
        }
        if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'>&raquo;</a></li>";
        echo "</ul>";
    }
}

function register_my_menu() {
    register_nav_menu('header-menu', 'Header Menu');
}

add_action('init', 'register_my_menu');

add_theme_support('post-thumbnails');

/*
 * Erbilen.net adresinden alınmıştır.
 * http://www.erbilen.net/php-timeago-fonksiyonu/
 */
function timeAgo($date)
{
    $timestamp = strtotime($date);
    $currentDate = new DateTime('@' . $timestamp);
    $nowDate = new DateTime('@' . time());
    $diff = $currentDate
        ->diff($nowDate);
    if ( $diff->y )
        return $diff->y . ' yıl önce';
    elseif ( $diff->m )
        return $diff->m . ' ay önce';
    elseif ( $diff->d )
        return $diff->d . ' gün önce';
}

/* Admin Page */
function my_admin_page() {
    add_menu_page('Social Icons', 'Theme Options', 'manage_options', 'my_admin_page', 'my_page_callback');
}

function my_page_callback() {
    echo '<form action="options.php" method="POST">';
    settings_fields('social_settings_group');
    do_settings_sections('my_admin_page');
    submit_button();
    echo '</form>';
}

add_action('admin_menu', 'my_admin_page');
add_action('admin_init', 'my_custom_settings');

function my_custom_settings() {
    register_setting('social_settings_group', 'facebook_handler');
    register_setting('social_settings_group', 'twitter_handler');
    register_setting('social_settings_group', 'youtube_handler');
    register_setting('social_settings_group', 'instagram_handler');
    register_setting('social_settings_group', 'codepen_handler');

    add_settings_section('my-custom-setting-section', 'Social Accounts', 'section_callback', 'my_admin_page');

    add_settings_field( 'facebook_handler', 'Facebook URL', 'facebook_callback', 'my_admin_page', 'my-custom-setting-section');
    add_settings_field( 'twitter_handler', 'Twitter URL', 'twitter_callback', 'my_admin_page', 'my-custom-setting-section');
    add_settings_field( 'youtube_handler', 'Youtube URL', 'youtube_callback', 'my_admin_page', 'my-custom-setting-section');
    add_settings_field( 'instagram_handler', 'Instagram URL', 'instagram_callback', 'my_admin_page', 'my-custom-setting-section');
    add_settings_field( 'codepen_handler', 'CodePen URL', 'codepen_callback', 'my_admin_page', 'my-custom-setting-section');
}

function section_callback() {
    echo '<h4>Social Accounts</h4>';
}

function facebook_callback() {
    $facebook = esc_attr( get_option('facebook_handler') );
    echo '<input type="text" name="facebook_handler" value="'.$facebook.'" placeholder="Facebook URL" />';
}

function twitter_callback() {
    $twitter = esc_attr( get_option('twitter_handler') );
    echo '<input type="text" name="twitter_handler" value="'.$twitter.'" placeholder="Twitter URL" />';
}

function youtube_callback() {
    $youtube = esc_attr( get_option('youtube_handler') );
    echo '<input type="text" name="youtube_handler" value="'.$youtube.'" placeholder="Youtube URL" />';
}

function instagram_callback() {
    $instagram = esc_attr( get_option('instagram_handler') );
    echo '<input type="text" name="instagram_handler" value="'.$instagram.'" placeholder="Instagram URL" />';
}

function codepen_callback() {
    $codepen = esc_attr( get_option('codepen_handler') );
    echo '<input type="text" name="codepen_handler" value="'.$codepen.'" placeholder="CodePen URL" />';
}