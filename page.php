<?php require "header.php"; the_post(); ?>

        <div class="header-text post-title">
            <h1><?php the_title(); ?></h1>
            <h4>By <?php the_author(); ?> On <em><?php the_date(); ?></em></h4>
        </div>

        <div class="header-image">
            <?php if(has_post_thumbnail(get_the_ID())): ?>
                <img src="<?php the_post_thumbnail("full"); ?>" alt="<?php the_title(); ?>">
            <?php else: ?>
                <img src="<?php bloginfo("template_url") ?>/images/header.jpg" alt="<?php the_title(); ?>">
            <?php endif; ?>
        </div>
    </header>

    <section id="blogs">
        <div class="container">
            <div class="blog">
                <h1><a href="#"><?php the_title() ?></a></h1>
                <?php the_content(); ?>
            </div>
            <?php include_once "comment_form.php"; ?>
            <?php comments_template('/comments.php'); ?>
        </div>
    </section>

<?php require "footer.php"; ?>