$(function() {

    $("#search").hide();

    $("#search form label input").on("focus", function() {
        $(this).parent().find('span').stop().animate({
            "bottom":260+"%",
            "font-size":2+"vh"
        }, 250);
    });

    $("#search form label input").on("blur", function() {
        if($(this).val() == "") {
            $(this).parent().find('span').stop().animate({
                "bottom": -15 + "px",
                "font-size": 3 + "vh"
            }, 250);
        }
    });

    $(".search").on("click", function() {
        $("#search").stop().slideDown().find("form label input").focus();
        return false;
    })

    $(".close").on("click", function() {
        $("#search").stop().slideUp().find('input').val("");
        return false;
    });

    $(document).on("keydown", function(e) {
        if(e.which == 27) {
            $("#search").stop().slideUp().find('input').val("");
        }
    });

    $(window).on("scroll", function() {
        var w = $(window).width();
        if(w > 760) {
            if($(window).scrollTop() > 350) {
                $("nav").css(
                    {
                        "position": "fixed",
                        "top":0,
                        "background":"rgba(0, 0, 0, .8)",
                        "left":0,
                        "z-index":"1",
                        "line-height":90+"px"
                    }
                );
            }else {
                $("nav").css(
                    {
                        "position":"static",
                        "background":"transparent"
                    }
                );
            }
        }
    });

    $(".navbar-button").on("click", function() {
        var toggle = $("nav").data("toggle");
        if(toggle == "opened") {
            $("nav").stop().animate({"left":"-100%"});
            $("nav").data("toggle", "closed");
        }else if(toggle == "closed") {
            $("nav").stop().animate({"left":0+"px"});
            $("nav").data("toggle", "opened");
        }

        return false;
    })
});