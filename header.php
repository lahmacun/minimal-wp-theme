<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <title><?php bloginfo("title"); echo " - ".get_the_title(); ?></title>
</head>
<body>

<div id="search">
    <a href="#" class="close"><i class="fa fa-close"></i></a>
    <form action="" method="GET">
        <label>
            <span>Type And Hit Enter</span>
            <input type="text" name="s" autocomplete="off">
        </label>
    </form>
    <div class="circle"></div>
</div>

<header>
    <div class="container">
        <a href="#" class="navbar-button"><i class="fa fa-bars"></i></a>
        <nav data-toggle="closed">
            <?php wp_nav_menu([
                "theme_location" => "header-menu",
                "container" => ""
            ]); ?>
            <ul class="search-list">
                <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
            </ul>
            <div class="clear"></div>
        </nav>
    </div>