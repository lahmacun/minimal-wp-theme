<?php // Do not delete these lines
if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die ('Please do not load this page directly. Thanks!');
if (!empty($post->post_password)) { // if there's a password
    if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
        ?>

        <p class="nocomments">This post is password protected. Enter the password to view comments.</p>

        <?php
        return;
    }
}
/* This variable is for alternating comment background */
$oddcomment = 'class="comments-alt" ';
?>

    <!-- You can start editing here. -->

<?php if ($comments) : $comments = array_reverse($comments); ?>
    <h2 id="comments" style="padding-left:25px;"><?php comments_number('Yorum Yok', 'Yorumlar', 'Yorumlar' );?></h2>

    <?php foreach ($comments as $comment) : ?>

        <div class="comment" id="comment-<?php comment_ID(); ?>">
            <div class="comment-avatar">
                <img src="http://gravatar.com/avatar/<?php echo md5(get_comment_author_email()); ?>" alt="Mustafa Zahid Efe">
            </div>
            <div class="comment-text">
                <span><em><?php comment_author_link(); ?></em> (<?php echo timeAgo(get_comment_date('Y-m-d H:i:s')); ?>)</span><br>
                <span>
                    <?php
                    if($comment->comment_parent != 0):
                        foreach($comments as $com) {
                            if($com->comment_ID == $comment->comment_parent) {
                                echo "<b>".$com->comment_author."</b> Adlı Kişinin <a href='#comment-".$com->comment_ID."'>yorumu</a>na Yanıt Olarak:";
                            }
                        }
                    endif;
                    ?>
                </span>
                <p><?php comment_text(); ?></p>
                <?php if($comment->comment_approved == '0'): ?>
                <em>Yorumunuz Onaylandıktan Sonra Yayınlanacak.</em>
                <?php endif; ?>
            </div>
        </div>

        <?php
        /* Changes every other comment to a different class */
        $oddcomment = ( empty( $oddcomment ) ) ? 'class="comments-alt" ' : '';
        ?>

    <?php endforeach; /* end for each comment */ ?>


<?php else : // this is displayed if there are no comments so far ?>

    <?php if ('open' == $post->comment_status) : ?>
        <!-- If comments are open, but there are no comments. -->

    <?php else : // comments are closed ?>
        <!-- If comments are closed. -->
    <?php endif; ?>
<?php endif; ?>